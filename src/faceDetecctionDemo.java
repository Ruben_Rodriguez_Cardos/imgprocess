import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class faceDetecctionDemo {

	public static final String XML_FILE = 
			"Resources/frontalface.xml";
	
	public static void main(String[] args) {
		
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		System.out.println("Running DetectFaceDemo");
		Mat img = null;
		try {
		    img = Imgcodecs.imread("Resources"+"\\"+"lena.png");
		    
		    CascadeClassifier faceDetector = new CascadeClassifier(XML_FILE);
		    
		    MatOfRect faceDetections = new MatOfRect();
		    faceDetector.detectMultiScale(img, faceDetections);
		    System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));
		    
		    // Draw a bounding box around each face.
		    for (Rect rect : faceDetections.toArray()) {
		        Imgproc.rectangle(img, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
		    }
		    
		    // Save the visualized detection.
		    String filename =  "Resources"+"\\"+"faceDetection.png";
		    System.out.println(String.format("Writing %s", filename));
		    Imgcodecs.imwrite(filename, img);
		    
		}catch(Exception ex){
			System.out.println(ex.toString());
		}
	}
}
