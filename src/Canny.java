import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Canny {
	public static void main(String[] args) {
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		
		Mat img = null;
		try {
		    img = Imgcodecs.imread("Resources"+"\\"+"lena.png");
		    
			Mat imageGray = new Mat();
			Mat imageCny = new Mat();
			Mat imagebw = new Mat();
			Mat imageBlur = new Mat();

			//Convert the image in to gray image single channel image
			Imgproc.cvtColor(img, imageGray, Imgproc.COLOR_BGR2GRAY);
			//Convert the image to B&W
			Imgproc.threshold(img, imagebw, 127, 255, Imgproc.THRESH_TOZERO);
			//Canny Edge Detection
			Imgproc.Canny(imageGray, imageCny, 10, 100, 3, true);
			//blur effect
			int distorsion = 32;//Mas alto menor distorsion
			Imgproc.blur(img, imageBlur, new Size(img.height()/distorsion,img.width()/distorsion));
			Imgproc.resize(imageBlur, imageBlur, new Size(img.height()/2,img.width()/2));
			
			// Save 
		    String filename = "Resources"+"\\"+"Blur.png";
		    System.out.println(String.format("Writing %s", filename));
		    Imgcodecs.imwrite(filename, imageBlur);	
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
