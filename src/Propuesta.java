import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Propuesta {

	public static void main(String[] args) {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat img = null;
		try {
			img = Imgcodecs.imread("Resources" + "\\" + "7Seg.png");
		}
		catch (Exception ex) {
			System.out.println(ex.toString());
		}
			//showMat(img, "Init - OpenCV 3.2");

			// Grayscale
			Mat img_grayscale = new Mat();
			Imgproc.cvtColor(img, img_grayscale, Imgproc.COLOR_BGR2GRAY);
			//showMat(img_grayscale, "Grayscale - OpenCV 3.2");

			// Binarized
			Mat img_binarized = new Mat();
			Imgproc.threshold(img_grayscale, img_binarized, 150, 250, Imgproc.THRESH_BINARY);
			//showMat(img_binarized, "Binarized - OpenCV 3.2");

			// Canny
			Mat img_canny = new Mat();
			Imgproc.Canny(img_binarized, img_canny, 75, 200);
			//showMat(img_canny, "Canny - OpenCV 3.2");

			// Crop & getWhitePixels
			/*for (int i = 0; i < 4; i++) {
				// Crop
				Rect crop;
				if ((((75) * i) + 75) > img_canny.width()) {
					crop = new Rect(0 + 75 * i, 0, img_canny.width() - ((0 + 75) * (i)), 85);
				} else {
					crop = new Rect(0 + 75 * i, 0, 75, 85);
				}
				Mat img_crop = new Mat(img_canny, crop);

				// Resize
				Size scale = new Size(img_crop.width() * 4, img_crop.height() * 4);
				Mat img_scale = new Mat();
				Imgproc.resize(img_crop, img_scale, scale);
				showMat(img_scale, "Crop & Scale " + i + " - OpenCV 3.2");

				// Get Pattern (White Pixels)
				List<Point> l = getWhitePixels(img_crop);
				System.out.println("Imagen " + i + " Numero de pixeles blancos: " + l.size());
				for(Point p : l){ System.out.println(p.x+","+p.y); }
				 
			}*/
			Rect crop;
			crop = new Rect(0 ,0, 75, 85);
			Mat img_crop = new Mat(img_canny, crop);
			showMat(img_crop, "Crop & Scale " + " - OpenCV 3.2");
			
			//Normalize
			Mat n = normalizeImg(img_crop);
			Size scale = new Size(n.width()*4,n.height()*4);
			Imgproc.resize(n,n, scale);
			showMat(n, "Normalized & Scale " + " - OpenCV 3.2");
			
			
			//Busco el pixel con la coordenada x mas baja
			/*List<Point> l = getWhitePixels(img_crop);
			int x_min = getXMin(l);
			System.out.println("X_min: "+x_min);
			
			//Busco el pixel con la coordenada x mas alta
			int x_max = getXMax(l);
			System.out.println("X_max: "+x_max);
			
			//Busco el pixel con la coordenada y mas baja
			int y_min = getYMin(l);
			System.out.println("Y_min: "+y_min);
			
			//Busco el pixel con la coordenada y mas alta
			int y_max = getYMax(l);
			System.out.println("Y_max: "+y_max);
			
			
			Mat aux = img_crop.clone();
			Rect r = new Rect(x_min,y_min,(x_max-x_min)+1,(y_max-y_min)+1);
			aux = new Mat(aux,r);
			Size scale = new Size(aux.width()*4,aux.height()*4);
			Imgproc.resize(aux, aux, scale);
			showMat(aux, "Normalized & Scale " + " - OpenCV 3.2");*/
		

	}
	
	private static Mat normalizeImg(Mat m){
		List<Point> l = getWhitePixels(m);
		int x_min = getXMin(l);
		System.out.println("X_min: "+x_min);
		
		//Busco el pixel con la coordenada x mas alta
		int x_max = getXMax(l);
		System.out.println("X_max: "+x_max);
		
		//Busco el pixel con la coordenada y mas baja
		int y_min = getYMin(l);
		System.out.println("Y_min: "+y_min);
		
		//Busco el pixel con la coordenada y mas alta
		int y_max = getYMax(l);
		System.out.println("Y_max: "+y_max);
		
		
		Mat aux = m.clone();
		Rect r = new Rect(x_min,y_min,(x_max-x_min)+1,(y_max-y_min)+1);
		aux = new Mat(aux,r);
		//Size scale = new Size(aux.width()*4,aux.height()*4);
		//Imgproc.resize(aux, aux, scale);
		return aux;
	}
	
	private static int getXMin(List<Point> l ){
		int x_min = Integer.MAX_VALUE;
		for(Point p : l){ 
			if(p.x<x_min){
				x_min=(int) p.x;
			}
		}
		return x_min;
	}
	
	private static int getXMax(List<Point> l ){
		int x_max = 0;
		for(Point p : l){ 
			if(p.x>x_max){
				x_max=(int) p.x;
			}
		}
		return x_max;
	}
	
	private static int getYMin(List<Point> l ){
		int y_min = Integer.MAX_VALUE;
		for(Point p : l){ 
			if(p.y<y_min){
				y_min=(int) p.y;
			}
		}
		return y_min;
	}
	
	private static int getYMax(List<Point> l ){
		int y_max = 0;
		for(Point p : l){ 
			if(p.y>y_max){
				y_max=(int) p.y;
			}
		}
		return y_max;
	}

	private static List<Point> getWhitePixels(Mat img_crop) {
		Mat aux = new Mat();
		Core.extractChannel(img_crop, aux, 0);
		Mat locations = new Mat();
		Core.findNonZero(aux, locations);
		return new MatOfPoint(locations).toList();
	}

	public static void showMat(Mat m, String Title) {
		displayImage(matToBufferedImage(m), Title);
	}

	private static void displayImage(Image img2, String Title) {
		// BufferedImage img=ImageIO.read(new File("/HelloOpenCV/lena.png"));
		ImageIcon icon = new ImageIcon(img2);
		JFrame frame = new JFrame(Title);
		frame.setLayout(new FlowLayout());
		frame.setSize(img2.getWidth(null) + 50, img2.getHeight(null) + 50);
		JLabel lbl = new JLabel();
		lbl.setIcon(icon);
		frame.add(lbl);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private static BufferedImage matToBufferedImage(Mat matrix) {
		int cols = matrix.cols();
		int rows = matrix.rows();
		int elemSize = (int) matrix.elemSize();
		byte[] data = new byte[cols * rows * elemSize];
		int type;

		matrix.get(0, 0, data);

		switch (matrix.channels()) {
		case 1:
			type = BufferedImage.TYPE_BYTE_GRAY;
			break;

		case 3:
			type = BufferedImage.TYPE_3BYTE_BGR;

			// bgr to rgb
			byte b;
			for (int i = 0; i < data.length; i = i + 3) {
				b = data[i];
				data[i] = data[i + 2];
				data[i + 2] = b;
			}
			break;

		default:
			return null;
		}

		BufferedImage image = new BufferedImage(cols, rows, type);
		image.getRaster().setDataElements(0, 0, cols, rows, data);

		return image;
	}

}
