import java.io.File;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class HelloOpenCV{
   public static void main( String[] args ){
      System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
      Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
      System.out.println( "mat = " + mat.dump() );
      File miDir = new File ("Resources");
      try {
    	  System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
    	  String[] aux = miDir.list();
    	  for(String i : aux){
    		  System.out.println(miDir.getCanonicalPath()+'\\'+i);
    	  }
    	  
      }
      catch(Exception e) {
    	  e.printStackTrace();
      }
   }
}
  