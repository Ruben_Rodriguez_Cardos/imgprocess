import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.FileChooserUI;

import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class ImgProcess extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private String path;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImgProcess frame = new ImgProcess();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ImgProcess() {
		setTitle("ImgProcess V 0.1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		textField = new JTextField();
		textField.setEditable(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 0;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);
		
		
		GridBagConstraints gbc_btnLoadImage = new GridBagConstraints();
		gbc_btnLoadImage.insets = new Insets(0, 0, 5, 5);
		gbc_btnLoadImage.gridx = 1;
		gbc_btnLoadImage.gridy = 0;
		
		
		GridBagConstraints gbc_btnToGrayScale = new GridBagConstraints();
		gbc_btnToGrayScale.insets = new Insets(0, 0, 5, 5);
		gbc_btnToGrayScale.gridx = 1;
		gbc_btnToGrayScale.gridy = 2;
		
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 2;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		JLabel lblImage = new JLabel("");
		scrollPane.setViewportView(lblImage);
		
		JButton btnToGrayScale = new JButton("To Gray Scale");
		btnToGrayScale.setEnabled(false);
		
		JButton btnLoadImage = new JButton("Load Image");
		btnLoadImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(ImgProcess.this);
		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            //This is where a real application would open the file.
		            //System.out.println("Opening: " + file.getName());
		            try {
						textField.setText(file.getCanonicalFile().getCanonicalPath());
						 BufferedImage img = ImageIO.read(fc.getSelectedFile());//it must be an image file, otherwise you'll get an exception
						 lblImage.setIcon(new ImageIcon(img));
						 if(btnToGrayScale.isEnabled()==false){
							 btnToGrayScale.setEnabled(true);
						 }
						 path=file.getCanonicalPath();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        } else {
		        	System.out.println("Opening cancelled: ");
		        }
			}
		});
		
		
		btnToGrayScale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				//Save Dialog
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save");   
				File fileToSave = null;
				int userSelection = fileChooser.showSaveDialog(ImgProcess.this);
				 
				if (userSelection == JFileChooser.APPROVE_OPTION) {
					fileToSave = fileChooser.getSelectedFile();
				    System.out.println("Save as file: " + fileToSave.getAbsolutePath());
				}
				
				//Mat img = null;
				try {
				    //img = Imgcodecs.imread(path);
					System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
					File input = new File(path);
					BufferedImage image = ImageIO.read(input);

					byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
					Mat img = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
					img.put(0, 0, data);

					Mat imageGray = new Mat();
					
					//Convert the image in to gray image single channel image
					Imgproc.cvtColor(img, imageGray, Imgproc.COLOR_BGR2GRAY);
					
					// Save 
				    String filename = fileToSave.getCanonicalPath();
				    System.out.println(String.format("Writing %s", filename));
				    Imgcodecs.imwrite(filename, imageGray);
				    
				    //load the image
				    File output = new File(fileToSave.getCanonicalPath());
				    BufferedImage res = ImageIO.read(output);//it must be an image file, otherwise you'll get an exception
					lblImage.setIcon(new ImageIcon(res));
				}
				catch (Exception ex) {
					// TODO Auto-generated catch block
					System.out.println(ex.toString());
				}
			}
		});
		
		contentPane.add(btnLoadImage, gbc_btnLoadImage);
		contentPane.add(btnToGrayScale, gbc_btnToGrayScale);
		
		
		
	}

}
